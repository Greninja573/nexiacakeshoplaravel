@extends('layout.templateadmin')
@section('title','Detail Message')
@section('isi')
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">Detail Message</h1>
        <form action="" method="get">
            <div class="form-group">
                <label for="">Fullname</label>
                <input type="text" name="fullname" id="fullname" class="form-control" value="{{ $contact->fullname }}" disabled>
            </div>
            <div class="form-group">
                <label for="">E-mail</label>
                <input type="text" name="email" id="email" class="form-control" value="{{ $contact->email }}" disabled>
            </div>
            <div class="form-group">
                <label for="">Phone Number</label>
                <input type="text" name="phonenumber" id="phonenumber" class="form-control" value="{{ $contact->phonenumber }}" disabled>
            </div>
            <div class="form-group">
                <label for="">Message</label>
                <textarea class="form-control" name="message" id="message" cols="10" rows="10" disabled style="resize: none">{{ $contact->message }}</textarea>
            </div>
        </form>
        <div class="buttons" style="margin-bottom: 20px;">
            <a href="/admin/" class="btn btn-secondary"><i class="fas fa-chevron-circle-left"></i> Back to List</a>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete-{{ $contact->id }}" class="btn btn-danger"><i class="fas fa-trash"></i> Delete Message</a>
        </div>    
    </div>
    <div class="col-md-2"></div>
</div>
@endsection
@section('modal')
@include('admin/deletemodal')
@endsection

